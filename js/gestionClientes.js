var clientesObtenidos;
function getClientes(){
  var url ="http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function()
  {
  if (this.readyState == 4 && this.status == 200){
    console.log(request.responseText);

    clientesObtenidos = request.responseText;
    procesarClientes();
  }
  }
  request.open("GET",url,true);
  request.send();
}


function procesarClientes(){
  var JSONClientes = JSON.parse(clientesObtenidos);
  var tabla =document.getElementById('tablaClientes');
  for (var i=0; i < JSONClientes.value.length; i++){
 var nuevaFila = document.createElement("tr");
 var columnaNombre = document.createElement("td");
 columnaNombre.innerText = JSONClientes.value[i].CompanyName;
 var columnaCiudad = document.createElement("td");
 columnaCiudad.innerText = JSONClientes.value[i].City;
 var columnaPais = document.createElement("td");
 columnaPais.innerText = JSONClientes.value[i].Country;
 var imagen = document.createElement("img");
 imagen.classList.add("flag");
 if (JSONClientes.value[i].Country=="UK")
 {
   imagen.src = " https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png";
 }
 else{
 imagen.src = " https://www.countries-ofthe-world.com/flags-normal/flag-of-"+JSONClientes.value[i].Country+".png";
}
  nuevaFila.appendChild(columnaNombre);
 nuevaFila.appendChild(columnaCiudad);
 nuevaFila.appendChild(imagen);
 tabla.appendChild(nuevaFila);

  }
}
